module.exports = async function (page, index, path) {
  if (path) {
    await page.screenshot({path: path + '/' + index + '.png'});
  }
}
