#!/usr/bin/env node
const { giteePublish } = require('../index')
const argv = require('minimist')(process.argv.slice(2))
const fs = require('fs')

if (argv['v'] || argv['version']) {
  return console.log(JSON.parse(fs.readFileSync(__dirname.replace('bin', 'package.json')).toString()).version)
}
let conf = {
  account: null,
  password: null,
  repository: null,
  outputimgpath: null
}
if (argv['conf']) {
  try {
    fs.accessSync(argv['conf'], fs.constants.R_OK)
    let json = JSON.parse(fs.readFileSync(argv['conf']))
    if (!json.account) return console.error('conf file missing account field')
    if (!json.password) return console.error('conf file missing password field')
    if (!json.repository) return console.error('conf file missing repository filed')
    conf.account = json.account
    conf.password = json.password
    conf.repository = json.repository
    if (json.outputimgpath) conf.outputimgpath = json.outputimgpath
  } catch (err) {
    console.error('conf path is not exit or conf.json is illegal')
    throw err
  }
} else {
  if (!(argv['c'] || argv['account'])) return console.log('Please enter gitee account | usage: [-c]/[--account]')
  if (!(argv['p'] || argv['password'])) return console.log('Please enter gitee password | usage: [-p]/[--password]')
  if (!(argv['r'] || argv['repository'])) return console.log('Please enter the name of the gitee repository to be deployed | usage: [-r]/[--repository]')
  conf.account = argv['c'] || argv['account']
  conf.password = argv['p'] || argv['password']
  conf.repository = argv['r'] || argv['repository']
  if (argv['o'] || argv['outputimgpath']) conf.outputimgpath = argv['o'] || argv['outputimgpath']
}
giteePublish(conf)
