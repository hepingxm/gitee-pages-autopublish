### 码云[gitee] 自动部署 pages服务

#### 因为码云的pages自动部署是需要收费的，so，开发了这个工具供大家使用

[源代码地址](https://gitee.com/hepingxm/gitee-pages-autopublish)

【注意】请先保证您已经手动部署过一次，然后才能自动部署！！  

[usage]  

1.先安装，执行  

npm i gitee-publish -g
// or
yarn global add gitee-publish

ps:如果出现下载 puppeteer 失败的情况，解决办法如下

 - 1.使用国外代理
 - 2.使用国内puppeteer源

2.使用配置文件的方式  
gitee-publish --conf 配置文件路径 【ps:只支持绝对路径】
 - 2.1 配置文件示例
 ~~~~
    {
      "account": "xxx", // gitee 账户
      "password": "xxx", // gitee密码
      "repository": "xxx", // 仓库名，请写全名，不支持模糊查询
      "outputimgpath": "" // 执行过程中的快照图片输出路径, 不填则不输出【ps:只支持绝对路径】
    }
~~~~
3.命令行参数方式  

~~~~
gitee-publish -c gitee账户 -p gitee密码 -r gitee仓库名 -o 执行过程中的快照图片输出路径  ~~~
// or
gitee-publish --account=gitee账户 --password=gitee密码 --repository=gitee仓库名 --outputimgpath=执行过程中的快照图片输出路径
~~~~
ps.如果你开启了快照输出路径，那么快照最后一张图片将输出如下  

![快照](http://heping.fun/gitstatic/gitee-publish/gitee-publish.png)

